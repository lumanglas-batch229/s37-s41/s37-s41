const Course = require("../models/Course");

// Create new course
/*
	Steps:
	1. Create a new Course object using the mongoose model
	2. Save the new Course to the database

*/

module.exports.addCourse = (data) => {

	// User is an admin
	if (data.isAdmin) {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		// Saves the created object to our database
		return newCourse.save().then((course, error) => {

			// Course creation successful
			if (error) {

				return false;

			// Course creation failed
			} else {

				return true;

			};

		});

	// User is not an admin
	} else {
		return false;
	};
	

};

// Retrieve all course
module.exports.getAllCourses = ()=> {
	return Course.find({}).then(result => {
		return result;
	})
}

// Retrieve a specific course

module.exports.getCourse = (reqParams) => {
	console.log(reqParams);
	return	Course.findById(reqParams.courseId).then(result => {
		return result;
	});
}

// Update a course
/*
Steps : 
1. Create a variable "updatedCourse" which will contain the info retrieved from the req body.
2. Find and updated the course using the course ID retrieved from the req params and the variable "updatedCourse"

*/
// info for updating will be coming from URL parameters and request body
module.exports.updateCourse = (reqParams, reqBody) => {
	
	// Specifiy the field of the document to be updated
	let updatedCourse = {
		name : reqBody.name,
		description: reqBody.description,
		price : reqBody.price
	};

	// findByIdUpdate (document ID, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then ((course, error) => {
		// Course not updated
		if (error) {
			return false;

		// Course updated successfully
		}else {
			return true;
		}
	})
}
// Activity

module.exports.archiveCourse = (data) => {
	if (data.isAdmin) {
		console.log(data)
		return Course.findByIdAndRemove(data).then ((archiveCourse, error) =>
		{
			if (error){
				return false;
			}else {
				return archiveCourse;
			}
		})
	}else {
		return res.send("Accessible by admin only")
	}
}


// SOLUTION in activity
module.exports.getAllActive = ()=> {
	return Course.find({isActive : true}).then(result => {
		return result;
	})
}