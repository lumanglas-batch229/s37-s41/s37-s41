const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth")

// Create course
router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));

});

router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

router.get("/", (req, res) => {
	courseController.getAllActive(req.body.isActive).then(resultFromController => res.send(resultFromController));
})

// Retrieve a specific course
// params for requesting at the URL 
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

// Update a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// Archive a course
router.put("/:courseId", auth.verify, (req, res) => {

	const data = {
		id: req.params.id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	courseController.archiveCourse(data).then(resultFromController => res.send(resultFromController));
})



// export the router object for index.js file
module.exports = router; 
